
<!DOCTYPE html>
<html lang="en-US" class="no-js">
<head>
	<meta charset="UTF-8" />
	<title>  Wedding Events | Arjun and Divya</title>
	<link rel="shortcut icon" href="/favicon.ico">
	<link rel="profile" href="http://gmpg.org/xfn/11" />
	<link rel="stylesheet" type="text/css" media="all" href="style.css" />

<!--[if lt IE 9]>
<script src="js/IE9.js"></script>
<![endif]-->
<!--[if lte IE 7]> <link href="css/ie.css" rel="stylesheet" type="text/css"><![endif]-->
<link rel="stylesheet" href="css/reveal.css">
<link rel='stylesheet' id='js_composer_front-css'  href='css/js_composer.css' type='text/css' media='all' />
<script type='text/javascript' src='//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js?ver=4.0'></script>
<style>
	body {
		visibility: hidden;
	}
</style>
<script>
	jQuery(document).ready(function(){
		delay();
	});

	function delay() {
		var secs = 3000;
		setTimeout('initFadeIn()', secs);
	}

	function initFadeIn() {
		jQuery("body").css("visibility","visible");
		jQuery("body").css("display","none");
		jQuery("body").fadeIn(3000);
	}
</script>
</head>

<body>
	<div id="header">
		<div id="masthead">
			<div id="branding">
				<a href="index.php" title="Arjun and Divya Home" rel="home"><img src="images/logo.png" alt="Arjun and Divya" /></a>
			</div>
			<div id="site_name">
				Arjun and Divya      
			</div>
			<div id="site_description">
				Tying the Knot 10th December, 2014  		
			</div>
		</div><!--end #masthead -->
	</div><!-- #header -->
	<div id="wrapper">
		<div id="main">
			<div id="posts">
				<div id="post-26" class="post-26 page type-page status-publish hentry background-image-1 post-title-3">
					<h1 class="entry_title" style="background-image:url(images/background-1.jpg)" >
						<span>Wedding Events</span>
					</h1>
					<div class="content">
						<div class="vc_row wpb_row vc_row-fluid">
							<div class="vc_col-sm-12 wpb_column vc_column_container">
								<div class="wpb_wrapper">

									<div class="wpb_single_image wpb_content_element wpb_animate_when_almost_visible wpb_top-to-bottom vc_align_left">
										<div class="wpb_wrapper">

											<img width="620" height="64" src="images/New age Jaanvasam.jpg" class=" vc_box_border_grey attachment-full" alt="Jaanvasam" />
										</div> 
									</div> <div class="vc_row wpb_row vc_inner vc_row-fluid">
									<div class="vc_col-sm-6 wpb_column vc_column_container">
										<div class="wpb_wrapper">
											<div class="wpb_gallery wpb_content_element vc_clearfix">
												<div class="wpb_wrapper"><div class="wpb_gallery_slides wpb_flexslider flexslider_fade flexslider" data-interval="3" data-flex_fx="fade"><ul class="slides" style="padding: 0px 5px 10px 5px;"><li><a class="prettyphoto" href="images/jaanvasam.jpg" rel="prettyPhoto[rel-2116512303]"><img width="198" height="254" src="images/jaanvasam-198x300.jpg" class="attachment-medium" alt="jaanvasam" style="width: 100%;height: 198px;" /></a></li></ul></div>
											</div> 
										</div> 
									</div> 
								</div> 

								<div class="vc_col-sm-6 wpb_column vc_column_container">
									<div class="wpb_wrapper">
										<div class="wpb_text_column wpb_content_element ">
											<div class="wpb_wrapper">
												<p class="font-dance">A tam-brahm equivalent to the North-Indian baarat, this ceremony forms the precursor to the wedding, welcoming the groom's party to venue on the eve of the wedding.</p>
												<p><b><span class="font-dance"><span class="aBn" tabindex="0" data-term="goog_1990416979"><span class="aQJ">9th December</span></span>, <i><span class="aBn" tabindex="0" data-term="goog_1990416980"><span class="aQJ">4:30 PM to 10 PM</span></span></i></span></b></p>
												<div><span class="font-dance"> </span></div>
												<div><span class="font-dance">Lavender Bough (next to Swaminarayan temple)</span></div>
												<div><span class="font-dance">90 Feet Road, Garodia Nagar</span></div>
												<div><span class="font-dance">Ghatkopar (East), Mumbai &#8211; 400077</span></div>
												<br>
												<div><span class="font-dance"><a href="#" class="big-link" data-reveal-id="newage">Click Here to view map</a></div>
												<div></div>
												<div></div>
											</div> 
										</div> 
									</div> 
								</div> 
							</div>
						</div> 
					</div> 
				</div><div class="vc_row wpb_row vc_row-fluid">
				<div class="vc_col-sm-12 wpb_column vc_column_container">
					<div class="wpb_wrapper">

						<div class="wpb_single_image wpb_content_element wpb_animate_when_almost_visible wpb_top-to-bottom vc_align_left">
							<div class="wpb_wrapper">

								<img width="620" height="64" src="images/Kalyanam_text.jpg" class=" vc_box_border_grey attachment-full" alt="960 Grid System: http://960.gs" />
							</div> 
						</div> <div class="vc_row wpb_row vc_inner vc_row-fluid">
						<div class="vc_col-sm-6 wpb_column vc_column_container">
							<div class="wpb_wrapper">

								<div class="wpb_gallery wpb_content_element vc_clearfix">
									<div class="wpb_wrapper"><div class="wpb_gallery_slides wpb_flexslider flexslider_fade flexslider" data-interval="3" data-flex_fx="fade"><ul class="slides" style="padding: 0px 5px 10px 5px;"><li><a class="prettyphoto" href="images/kalyanam.jpg" rel="prettyPhoto[rel-1090073806]"><img width="300" height="200" src="images/kalyanam-300x200.jpg" class="attachment-medium" alt="kalyanam" style="width: 100%;height: 177px;"/></a></li></ul></div>
								</div> 
							</div> 
						</div> 
					</div> 

					<div class="vc_col-sm-6 wpb_column vc_column_container">
						<div class="wpb_wrapper">

							<div class="wpb_text_column wpb_content_element ">
								<div class="wpb_wrapper">
									<p class="font-dance">The traditional tamil wedding, sanctioning the holy union of two persons, with the blessings of elders and best wishes from all for a lifetime of happiness together.</p>
									<p><b><span class="font-dance"><span class="aBn" tabindex="0" data-term="goog_1990416981"><span class="aQJ">10th December</span></span>, <i><span class="aBn" tabindex="0" data-term="goog_1990416982"><span class="aQJ">6:30 AM to 2 PM</span></span></i></span></b></p>
									<div><span class="font-dance"> </span></div>
									<div><span class="font-dance">Lavender Bough (next to Swaminarayan temple)</span></div>
									<div><span class="font-dance">90 Feet Road, Garodia Nagar</span></div>
									<div><span class="font-dance">Ghatkopar (East), Mumbai &#8211; 400077</span></div>
									<br>
									<div><span class="font-dance"><a href="#" class="big-link" data-reveal-id="kalya">Click Here to view map</a></div>
									<div></div>
									<div></div>
								</div> 
							</div> 
						</div> 
					</div> 
				</div>
			</div> 
		</div> 
	</div><div class="vc_row wpb_row vc_row-fluid">
	<div class="vc_col-sm-12 wpb_column vc_column_container">
		<div class="wpb_wrapper">
			
			<div class="wpb_single_image wpb_content_element wpb_animate_when_almost_visible wpb_top-to-bottom vc_align_left">
				<div class="wpb_wrapper">

					<img width="620" height="64" src="images/Reception---Bombay.jpg" class=" vc_box_border_grey attachment-full" alt="960 Grid System: http://960.gs" />
				</div> 
			</div> <div class="vc_row wpb_row vc_inner vc_row-fluid">
			<div class="vc_col-sm-6 wpb_column vc_column_container">
				<div class="wpb_wrapper">

					<div class="wpb_gallery wpb_content_element vc_clearfix">
						<div class="wpb_wrapper"><div class="wpb_gallery_slides wpb_flexslider flexslider_fade flexslider" data-interval="3" data-flex_fx="fade"><ul class="slides" style="padding: 0px 5px 10px 5px;"><li><a class="prettyphoto" href="images/receptionb.jpg" rel="prettyPhoto[rel-1463035171]"><img width="300" height="130" src="images/receptionb-300x106.jpg" class="attachment-medium" alt="receptionb" style="width: 100%;height: 177px;" /></a></li></ul></div>
					</div> 
				</div> 
			</div> 
		</div> 

		<div class="vc_col-sm-6 wpb_column vc_column_container">
			<div class="wpb_wrapper">

				<div class="wpb_text_column wpb_content_element ">
					<div class="wpb_wrapper">
						<p class="font-dance">With a nod to the ultimate symbol of romance and the spirit of this city,<br>You're warmly welcome to the reception of the happy couple amidst the sea-side beauty.</p>
						<p><b><span class="font-dance"><span class="aBn" tabindex="0" data-term="goog_1990416983"><span class="aQJ">11th December</span></span>, <i><span class="aBn" tabindex="0" data-term="goog_1990416984"><span class="aQJ">7:30 PM</span></span> onwards</i></span></b></p>
						
						<div><span class="font-dance"> </span></div>
						<div>
							<div><span class="font-dance">Hotel Sea Princess</span></div>
							<div><span class="font-dance">Beach front, Juhu Beach, </span></div>
							<div><span class="font-dance">Juhu, Mumbai</span></div>
						</div>
						<br>
						<div><span class="font-dance"><a href="#" class="big-link" data-reveal-id="recbom">Click Here to view map</a></div>
						<div></div>
						<div></div>

					</div> 
				</div> 
			</div> 
		</div> 
	</div>
</div> 
</div> 
</div><div class="vc_row wpb_row vc_row-fluid">
<div class="vc_col-sm-12 wpb_column vc_column_container">
	<div class="wpb_wrapper">

		<div class="wpb_single_image wpb_content_element wpb_animate_when_almost_visible wpb_top-to-bottom vc_align_left">
			<div class="wpb_wrapper">
				<img width="620" height="64" src="images/Reception---Hyderabad.jpg" class=" vc_box_border_grey attachment-full" alt="960 Grid System: http://960.gs" />
			</div> 
		</div> <div class="vc_row wpb_row vc_inner vc_row-fluid">
		<div class="vc_col-sm-6 wpb_column vc_column_container">
			<div class="wpb_wrapper">

				<div class="wpb_gallery wpb_content_element vc_clearfix">
					<div class="wpb_wrapper"><div class="wpb_gallery_slides wpb_flexslider flexslider_fade flexslider" data-interval="3" data-flex_fx="fade"><ul class="slides" style="padding: 0px 5px 10px 5px;"><li><a class="prettyphoto" href="images/receptionh_full.jpg" rel="prettyPhoto[rel-791126384]"><img width="300" height="189" src="images/receptionh-300x189.jpg" class="attachment-medium" alt="receptionh" style="width: 100%;height: 176px;" /></a></li></ul></div>
				</div> 
			</div> 
		</div> 
	</div> 

	<div class="vc_col-sm-6 wpb_column vc_column_container">
		<div class="wpb_wrapper">
			
			<div class="wpb_text_column wpb_content_element ">
				<div class="wpb_wrapper">
					<p class="font-dance">You're cordially invited for a regal evening of musical fusion,<br>In the city of Nizams to celebrate the new couple's reception.</p>
					<p><b><span class="font-dance"><span class="aBn" tabindex="0" data-term="goog_1990416985"><span class="aQJ">13th December</span></span>, <i><span class="aBn" tabindex="0" data-term="goog_1990416986"><span class="aQJ">7 PM</span></span> onwards</i></span></b></p>
					<div><span class="font-dance"> </span></div>
					<div>
						<div><span class="font-dance">Vedika Hall, Jalavihar,</span></div>
						<div><span class="font-dance"> Necklace Road,</span></div>
						<div><span class="font-dance">Hussain Sagar, Hyderabad.</span></div>						
					</div>
					<br>
					<div><span class="font-dance"><a href="#" class="big-link" data-reveal-id="rechyd">Click Here to view map</a></div>
					<div></div>
					<div></div>

				</div> 
			</div> 
		</div> 
	</div> 
</div>
</div> 
</div> 
</div>
</div>
</div> <!--end .post -->
<div class="post_end"></div>
</div>
</div><!--end #main -->

</div><!--end #wrapper -->
<div id="menu-main-menu-container" class="menu-main-menu-container">
	<ul id="menu-main-menu" class="menu">
		<li class="main-menu-item post-title-1"><a href="our-story.php" class="menu-link" >Our Story</a></li>
		<li class="main-menu-item post-title-2"><a href="family.php" class="menu-link" >Our Family</a></li>
		<li class="main-menu-item post-title-3"><a href="wedding-events.php" class="menu-link" >Wedding Events</a></li>
			<li class="main-menu-item post-title-4"><a href="photos.php" class="menu-link" >Photos</a></li>
		<li class="main-menu-item post-title-5"><a href="e-vite.php" class="menu-link" >E-vite</a></li>
		<li class="main-menu-item post-title-6"><a href="engagement.php" class="menu-link" >Engagement</a></li>
	</ul>
</div>

<div id="footer">
	<div id="footer_content">
		<div id="footer_icon"></div>
	</div>
</div>
<div id="newage" class="reveal-modal">
	<h1 style="margin-bottom: 5px;">Location</h1>
	<img src="images/kalyanamlayout.png" style="width: 100%;">
</div>
<div id="kalya" class="reveal-modal">
	<h1 style="margin-bottom: 5px;">Location</h1>
	<img src="images/kalyanamlayout.png" style="width: 100%;">
</div>
<div id="recbom" class="reveal-modal">
	<h1 style="margin-bottom: 5px;">Location</h1>
	<img src="images/receptionblayout.png" style="width: 100%;">
</div>
<div id="rechyd" class="reveal-modal">
	<h1 style="margin-bottom: 5px;">Location</h1>
	<img src="images/receptionblayout.jpg" style="width: 100%;">
</div>
<script type="text/javascript" src="http://code.jquery.com/jquery-1.6.min.js"></script>
<script type="text/javascript" src="js/jquery.reveal.js"></script>
</body>
</html>