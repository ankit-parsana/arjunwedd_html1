
<!DOCTYPE html>
<html lang="en-US" class="no-js">
<head>
	<meta charset="UTF-8" />
	<title>  Photos | Arjun and Divya</title>
	<link rel="shortcut icon" href="/favicon.ico">
	<link rel="profile" href="http://gmpg.org/xfn/11" />
	<link rel="stylesheet" type="text/css" media="all" href="style.css" />

<!--[if lt IE 9]>
<script src="js/IE9.js"></script>
<![endif]-->
<!--[if lte IE 7]> <link href="css/ie.css" rel="stylesheet" type="text/css"><![endif]-->

<link rel='stylesheet' id='js_composer_front-css'  href='css/js_composer.css' type='text/css' media='all' />
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<link rel='stylesheet' href='css/justifiedGallery.css' type='text/css' media='all' />
<script src='js/justifiedGallery1.js'></script>
<link rel='stylesheet' href='jquery-colorbox/colorbox.css' type='text/css' media='screen' />
<style>
	body {
		visibility: hidden;
	}
</style>
<script src='jquery-colorbox/jquery.colorbox-min.js'></script>
<script>
	jQuery(document).ready(function(){
		delay();
	});

	function delay() {
		var secs = 4000;
		setTimeout('initFadeIn()', secs);
	}

	function initFadeIn() {
		jQuery("body").css("visibility","visible");
		jQuery("body").css("display","none");
		jQuery("body").fadeIn(4000);
	}

	$(document).ready(function () {
		$(".colorboxEx").each(function (i, el) {
			$(el).justifiedGallery({rel: 'gal' + i}).on('jg.complete', function () {
				$(this).find('a').colorbox({
					maxWidth : '80%',
					maxHeight : '80%',
					opacity : 0.8,
					transition : 'elastic',
					current : ''
				});
			});
		});
	});
</script>
</head>

<body>
	<div id="header">
		<div id="masthead">
			<div id="branding">
				<a href="index.php" title="Arjun and Divya Home" rel="home"><img src="images/logo.png" alt="Arjun and Divya" /></a>
			</div>
			<div id="site_name">
				Arjun and Divya      </div>
				<div id="site_description">
					Tying the Knot 10th December, 2014  		
				</div>
			</div><!--end #masthead -->
		</div><!-- #header -->
		<div id="wrapper">
			<div id="main">
				<div id="posts">
					<div id="post-24" class="post-24 page type-page status-publish hentry background-image-2 post-title-4">
						<h1 class="entry_title" style="background-image:url(images/background-1.jpg)" >
							<span>Our Family</span>
						</h1>
						<div class="content">
							<div class="vc_row wpb_row vc_row-fluid">
								<div class="vc_col-sm-12 wpb_column vc_column_container">
									<div class="wpb_wrapper">
										<div class="colorboxEx">
											<?php for($i=1;$i<=14;$i++) { ?>
											<a href="photos/photos-<?php echo $i; ?>.jpg">
												<img src="photos/thumb/photos-<?php echo $i; ?>.jpg" />
											</a>
											<?php } ?>
										</div>
									</div> 
								</div>
							</div>
						</div> <!--end .post -->
						<div class="post_end"></div>
					</div>
				</div><!--end #main -->
			</div><!--end #wrapper -->
			<div id="menu-main-menu-container" class="menu-main-menu-container">
				<ul id="menu-main-menu" class="menu">
					<li class="main-menu-item post-title-1"><a href="our-story.php" class="menu-link" >Our Story</a></li>
					<li class="main-menu-item post-title-2"><a href="family.php" class="menu-link" >Our Family</a></li>
					<li class="main-menu-item post-title-3"><a href="wedding-events.php" class="menu-link" >Wedding Events</a></li>
					<li class="main-menu-item post-title-4"><a href="photos.php" class="menu-link" >Photos</a></li>
					<li class="main-menu-item post-title-5"><a href="e-vite.php" class="menu-link" >E-vite</a></li>
					<li class="main-menu-item post-title-6"><a href="engagement.php" class="menu-link" >Engagement</a></li>
				</ul>
			</div>

			<div id="footer">
				<div id="footer_content">
					<div id="footer_icon"></div>
				</div>
			</div>
			<img class="loading_icon" src="images/loading.gif" />
		</body>
		</html>