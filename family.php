
<!DOCTYPE html>
<html lang="en-US" class="no-js">
<head>
	<meta charset="UTF-8" />
	<title>  Our Family | Arjun and Divya</title>
	<link rel="shortcut icon" href="/favicon.ico">
	<link rel="profile" href="http://gmpg.org/xfn/11" />
	<link rel="stylesheet" type="text/css" media="all" href="style.css" />

<!--[if lt IE 9]>
<script src="js/IE9.js"></script>
<![endif]-->
<!--[if lte IE 7]> <link href="css/ie.css" rel="stylesheet" type="text/css"><![endif]-->

<link rel='stylesheet' id='js_composer_front-css'  href='css/js_composer.css' type='text/css' media='all' />
<script type='text/javascript' src='//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js?ver=4.0'></script>
<style>
	body {
		visibility: hidden;
	}
</style>
<script>
	jQuery(document).ready(function(){
		delay();
	});

	function delay() {
		var secs = 4000;
		setTimeout('initFadeIn()', secs);
	}

	function initFadeIn() {
		jQuery("body").css("visibility","visible");
		jQuery("body").css("display","none");
		jQuery("body").fadeIn(4000);
	}
</script>
</head>

<body>
	<div id="header">
		<div id="masthead">
			<div id="branding">
				<a href="index.php" title="Arjun and Divya Home" rel="home"><img src="images/logo.png" alt="Arjun and Divya" /></a>
			</div>
			<div id="site_name">
				Arjun and Divya      </div>
				<div id="site_description">
					Tying the Knot 10th December, 2014  		</div>
				</div><!--end #masthead -->
			</div><!-- #header -->
			<div id="wrapper">
				<div id="main">
					<div id="posts">
						<div id="post-24" class="post-24 page type-page status-publish hentry background-image-2 post-title-2">
							<h1 class="entry_title" style="background-image:url(images/background-22.jpg)" >
								<span>Our Family</span>
							</h1>
							<div class="content">
								<div class="vc_row wpb_row vc_row-fluid">
									<div class="vc_col-sm-12 wpb_column vc_column_container">
										<div class="wpb_wrapper">
											<div class="cqtooltip-wrapper " data-opacity="0" data-tooltipanimation="grow" data-tooltipstyle="shadow" data-trigger="hover" data-maxwidth="400px">
												<img src="images/family-tree1.jpg" style="width: 122% !important;margin-left: -68px !important;" />
												<div class="cq-hotspots">
													<div class="hotspot-item " style="top:44.5%;left:0%;">
														<a href="#" class="cq-tooltip" style="background:rgba(255,255,255,0.01);" data-tooltip="&lt;img id=&quot;__wp-temp-img-id&quot; src=&quot;images/gp1.jpg&quot; alt=&quot;&quot; width=&quot;400&quot; height=&quot;267&quot; /&gt; " data-arrowposition="left">
															<span style="background:rgba(255,255,255,0.01);"></span>
														</a>
													</div>
													<div class="hotspot-item " style="top:28.5%;left:0%;">
														<a href="#" class="cq-tooltip" style="background:rgba(255,255,255,0.01);" data-tooltip="&lt;img id=&quot;__wp-temp-img-id&quot; src=&quot;images/gp2.jpg&quot; alt=&quot;&quot; width=&quot;400&quot; height=&quot;267&quot; /&gt; " data-arrowposition="left">
															<span style="background:rgba(255,255,255,0.01);"></span>
														</a>
													</div>
													<div class="hotspot-item " style="top:22.5%;left:23%;">
														<a href="#" class="cq-tooltip" style="background:rgba(255,255,255,0.01);" data-tooltip="&lt;img id=&quot;__wp-temp-img-id&quot; src=&quot;images/patti.jpg&quot; alt=&quot;&quot; width=&quot;400&quot; height=&quot;267&quot; /&gt; " data-arrowposition="left">
															<span style="background:rgba(255,255,255,0.01);"></span>
														</a>
													</div>
													<div class="hotspot-item " style="top:26%;left:61%">
														<a href="#" class="cq-tooltip" style="background:rgba(255,255,255,0.01);" data-tooltip="&lt;img id=&quot;__wp-temp-img-id&quot; src=&quot;images/pattis.jpg&quot; alt=&quot;&quot; width=&quot;400&quot; height=&quot;267&quot; /&gt; " data-arrowposition="left">
															<span style="background:rgba(255,255,255,0.01);"></span>
														</a>
													</div>
													<div class="hotspot-item " style="top:46.5%;left:40.5%;">
														<a href="#" class="cq-tooltip" style="background:rgba(255,255,255,0.01);" data-tooltip="&lt;img id=&quot;__wp-temp-img-id&quot; src=&quot;images/ammaappa.jpg&quot; alt=&quot;&quot; width=&quot;400&quot; height=&quot;267&quot; /&gt; " data-arrowposition="left">
															<span style="background:rgba(255,255,255,0.01);"></span>
														</a>
													</div>
													<div class="hotspot-item " style="top:41.5%;left:84.5%%;">
														<a href="#" class="cq-tooltip" style="background:rgba(255,255,255,0.01);" data-tooltip="&lt;img id=&quot;__wp-temp-img-id&quot; src=&quot;images/mildil.jpg&quot; alt=&quot;&quot; width=&quot;400&quot; height=&quot;267&quot; /&gt; " data-arrowposition="left">
															<span style="background:rgba(255,255,255,0.01);"></span>
														</a>
													</div>
													<div class="hotspot-item " style="top:60.5%;left:11.5%%;">
														<a href="#" class="cq-tooltip" style="background:rgba(255,255,255,0.01);" data-tooltip="&lt;img id=&quot;__wp-temp-img-id&quot; src=&quot;images/sharan.jpg&quot; alt=&quot;&quot; width=&quot;400&quot; height=&quot;480&quot; /&gt; " data-arrowposition="left">
															<span style="background:rgba(255,255,255,0.01);"></span>
														</a>
													</div>
													<div class="hotspot-item " style="top:62.5%;left:63%;">
														<a href="#" class="cq-tooltip" style="background:rgba(255,255,255,0.01);" data-tooltip="&lt;img id=&quot;__wp-temp-img-id&quot; src=&quot;images/asharj.jpg&quot; alt=&quot;&quot; width=&quot;400&quot; height=&quot;480&quot; /&gt; " data-arrowposition="left">
															<span style="background:rgba(255,255,255,0.01);"></span>
														</a>
													</div>
													<div class="hotspot-item " style="top:77%;left:40%;">
														<a href="#" class="cq-tooltip" style="background:rgba(255,255,255,0.01);" data-tooltip="&lt;img id=&quot;__wp-temp-img-id&quot; src=&quot;images/our-story1.jpg&quot; alt=&quot;&quot; width=&quot;400&quot; height=&quot;480&quot; /&gt; " data-arrowposition="left">
															<span style="background:rgba(255,255,255,0.01);"></span>
														</a>
													</div>
												</div>
											</div>
										</div> 
									</div> 
								</div>
							</div>
						</div> <!--end .post -->
						<div class="post_end"></div>
					</div>
				</div><!--end #main -->
			</div><!--end #wrapper -->
			<div id="menu-main-menu-container" class="menu-main-menu-container">
				<ul id="menu-main-menu" class="menu">
					<li class="main-menu-item post-title-1"><a href="our-story.php" class="menu-link" >Our Story</a></li>
					<li class="main-menu-item post-title-2"><a href="family.php" class="menu-link" >Our Family</a></li>
					<li class="main-menu-item post-title-3"><a href="wedding-events.php" class="menu-link" >Wedding Events</a></li>
					<li class="main-menu-item post-title-4"><a href="photos.php" class="menu-link" >Photos</a></li>
					<li class="main-menu-item post-title-5"><a href="e-vite.php" class="menu-link" >E-vite</a></li>
					<li class="main-menu-item post-title-6"><a href="engagement.php" class="menu-link" >Engagement</a></li>
				</ul>
			</div>

			<div id="footer">
				<div id="footer_content">
					<div id="footer_icon"></div>
				</div>
			</div>
			<img class="loading_icon" src="images/loading.gif" />
			<link rel='stylesheet' id='vc_hotspot_cq_style-css'  href='css/vc/style.min.css?ver=4.0' type='text/css' media='all' />
			<link rel='stylesheet' id='tooltipster-css'  href='css/vc/tooltipster.css?ver=4.0' type='text/css' media='all' />
			<script type='text/javascript' src='js/vc/jquery.tooltipster.min.js?ver=4.0'></script>
			<script type='text/javascript' src='js/vc/script.min.js?ver=4.0'></script>
		</body>
		</html>