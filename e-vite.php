
<!DOCTYPE html>
<html lang="en-US" class="no-js">
<head>
	<meta charset="UTF-8" />
	<title>  e-vite | Arjun and Divya</title>
	<link rel="shortcut icon" href="/favicon.ico">
	<link rel="profile" href="http://gmpg.org/xfn/11" />
	<link rel="stylesheet" type="text/css" media="all" href="style.css" />

<!--[if lt IE 9]>
<script src="js/IE9.js"></script>
<![endif]-->
<!--[if lte IE 7]> <link href="css/ie.css" rel="stylesheet" type="text/css"><![endif]-->

<link rel='stylesheet' id='js_composer_front-css'  href='css/js_composer.css' type='text/css' media='all' />
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<style>
	body {
		visibility: hidden;
	}
</style>
<script>
	jQuery(document).ready(function(){
		delay();
	});

	function delay() {
		var secs = 3000;
		setTimeout('initFadeIn()', secs);
	}

	function initFadeIn() {
		jQuery("body").css("visibility","visible");
		jQuery("body").css("display","none");
		jQuery("body").fadeIn(3000);
	}
</script>
</head>

<body>
	<div id="header">
		<div id="masthead">
			<div id="branding">
				<a href="index.php" title="Arjun and Divya Home" rel="home"><img src="images/logo.png" alt="Arjun and Divya" /></a>
			</div>
			<div id="site_name">
				Arjun and Divya      
			</div>
			<div id="site_description">
				Tying the Knot 10th December, 2014  		
			</div>
		</div><!--end #masthead -->
	</div><!-- #header -->
	<div id="wrapper">
		<div id="main">
			<div id="posts">
				<div id="post-24" class="post-24 page type-page status-publish hentry background-image-2 post-title-5">
					<h1 class="entry_title" style="background-image:url(images/background-32.jpg)" >
						<span>Our Family</span>
					</h1>
					<div class="content">
						<div class="vc_row wpb_row vc_row-fluid">
							<div class="vc_col-sm-12 wpb_column vc_column_container">
								<div class="wpb_wrapper">
									<div class="wpb_single_image wpb_content_element wpb_animate_when_almost_visible wpb_left-to-right vc_align_left">
										<div class="wpb_wrapper" style="text-align: center;">
											<img src="images/EVITE1.jpg" class=" vc_box_border_grey attachment-full" alt="evite"/>
										</div> 
									</div> 
								</div> 
							</div>
							<div class="vc_col-sm-12 wpb_column vc_column_container">
								<div class="wpb_wrapper">
									<div class="wpb_single_image wpb_content_element wpb_animate_when_almost_visible wpb_left-to-right vc_align_left">
										<div class="wpb_wrapper" style="text-align: center;">
											<img src="images/EVITE2.jpg" class=" vc_box_border_grey attachment-full" alt="evite" />
										</div> 
									</div> 
								</div> 
							</div> 

							<!-- <div class="vc_col-sm-6 wpb_column vc_column_container">
								<div class="wpb_wrapper">
									<div class="wpb_raw_code wpb_content_element wpb_raw_html" style="margin-bottom: 0px;">
										<div class="wpb_wrapper">
											<h2 style="background: transparent;color: #f00;margin-bottom: 0px;"><marquee style="font-size: 22px;font-weight: 800;font-family: book antiqua;" scrollamount="4">You are Invited</marquee></h2>
										</div> 
									</div> 
									<div class="wpb_text_column wpb_content_element " style="margin-bottom: 10px;">
										<div class="wpb_wrapper">
											<p style="text-align: center;"><span style="font-family: 'book antiqua', palatino; font-size: 14pt; color: #008080;">Join the celebration, </span><br />
												<span style="font-family: 'book antiqua', palatino; font-size: 14pt; color: #008080;"> on our wedding day. </span><br />
												<span style="font-family: 'book antiqua', palatino; font-size: 14pt; color: #008080;"> Our mutual love, </span><br />
												<span style="font-family: 'book antiqua', palatino; font-size: 14pt; color: #008080;"> we&#8217;ll openly convey. </span><br />
												<span style="font-family: 'book antiqua', palatino; font-size: 14pt; color: #008080;"> Joy and excitement, </span><br />
												<span style="font-family: 'book antiqua', palatino; font-size: 14pt; color: #008080;"> gladly we&#8217;ll share. </span><br />
												<span style="font-family: 'book antiqua', palatino; font-size: 14pt; color: #008080;"> It will be truly wonderful, </span><br />
												<span style="font-family: 'book antiqua', palatino; font-size: 14pt; color: #008080;"> as long as you&#8217;re there.</span></p>

											</div> 
										</div> 
										<div class="wpb_raw_code wpb_content_element wpb_raw_html">
											<div class="wpb_wrapper">
												<h2 style="background: transparent;color: #f00;margin-bottom: 0px;"><marquee style="font-size: 22px;font-weight: 800;font-family: book antiqua;" scrollamount="4">Save the date - 10 December 2014 and join us in the celebration</marquee></h2>
											</div> 
										</div> 
									</div> 
								</div> 
							</div> -->
						</div>
					</div> <!--end .post -->
					<div class="post_end"></div>
				</div>
			</div><!--end #main -->

		</div><!--end #wrapper -->
		<div id="menu-main-menu-container" class="menu-main-menu-container">
			<ul id="menu-main-menu" class="menu">
				<li class="main-menu-item post-title-1"><a href="our-story.php" class="menu-link" >Our Story</a></li>
				<li class="main-menu-item post-title-2"><a href="family.php" class="menu-link" >Our Family</a></li>
				<li class="main-menu-item post-title-3"><a href="wedding-events.php" class="menu-link" >Wedding Events</a></li>
			<li class="main-menu-item post-title-4"><a href="photos.php" class="menu-link" >Photos</a></li>
				<li class="main-menu-item post-title-5"><a href="e-vite.php" class="menu-link" >E-vite</a></li>
				<li class="main-menu-item post-title-6"><a href="engagement.php" class="menu-link" >Engagement</a></li>
			</ul>
		</div>

		<div id="footer">
			<div id="footer_content">
				<div id="footer_icon"></div>
			</div>
		</div>
	</body>
	</html>