
<!DOCTYPE html>
<html lang="en-US" class="no-js">
<head>
	<meta charset="UTF-8" />
	<title>  Our Story | Arjun and Divya</title>
	<link rel="shortcut icon" href="/favicon.ico">
	<link rel="profile" href="http://gmpg.org/xfn/11" />
	<link rel="stylesheet" type="text/css" media="all" href="style.css" />

<!--[if lt IE 9]>
<script src="js/IE9.js"></script>
<![endif]-->
<!--[if lte IE 7]> <link href="css/ie.css" rel="stylesheet" type="text/css"><![endif]-->


<link rel='stylesheet' id='js_composer_front-css'  href='css/js_composer.css' type='text/css' media='all' />
<script type='text/javascript' src='//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js?ver=4.0'></script>
<style>
	body {
		visibility: hidden;
	}
</style>
<script>
	jQuery(document).ready(function(){
		delay();
	});

	function delay() {
		var secs = 1000;
		setTimeout('initFadeIn()', secs);
	}

	function initFadeIn() {
		jQuery("body").css("visibility","visible");
		jQuery("body").css("display","none");
		jQuery("body").fadeIn(1000);
	}
</script>
</head>

<body>
	<div id="header">
		<div id="masthead">
			<div id="branding">
				<a href="index.php" title="Arjun and Divya Home" rel="home"><img src="images/logo.png" alt="Arjun and Divya" /></a>
			</div>
			<div id="site_name">
				Arjun and Divya      
			</div>
			<div id="site_description">
				Tying the Knot 10th December, 2014  		
			</div>
		</div><!--end #masthead -->
	</div><!-- #header -->
	<div id="wrapper">
		<div id="main">
			<div id="posts">
				<div id="post-2" class="post-2 page type-page status-publish hentry background-image-3 post-title-1">
					<h1 class="entry_title" style="background-image:url(images/background-32.jpg)" >
						<span>Our Story</span>
					</h1>
					<div class="content">
						<div class="vc_row wpb_row vc_row-fluid">
							<div class="vc_col-sm-12 wpb_column vc_column_container">
								<div class="wpb_wrapper">
									<div class="wpb_single_image wpb_content_element wpb_animate_when_almost_visible wpb_left-to-right vc_align_left">
										<div class="wpb_wrapper">
											<a href="#" target="_self"><img class=" vc_box_border_grey " src="images/our-story.jpg" /></a>
										</div> 
									</div> 
								</div> 
							</div> 
						</div>
						<div class="vc_row wpb_row vc_row-fluid">
							<div class="vc_col-sm-12 wpb_column vc_column_container">
								<div class="wpb_wrapper">
									<div class="wpb_text_column wpb_content_element" style="margin-bottom: 20px;">
										<div class="wpb_wrapper">
											<p class="font-dance" style="text-align: center;font-weight:900;">
												<b>And thus it came upon, a match so right;<br>
													Devised by God, aided by a matrimonial site!<br>
													For wondrous it is, that a few virtual dates <br>
													could lead to an affection so driven,<br>
													that soon enough, the world was about the other's presence.<br>
													By all else, their bliss was sure forgiven,<br>
													For isn't incivility at the heart of love's essence?<br>
													Never having known such comfort, <br>
													a feeling so profound or pure;<br>
													A promise to love, honour and cherish eternally,<br>
													of this, I thee assure.<br>
													The most joyful moments experienced when we did court,<br>
													get close, getting past the intrigue and the mystery.<br>
													Here we are today, looking at a lifetime of love and support,<br>
													and the rest as they say, is history!</b>
												</p>
											</div> 
										</div>
										<div class="wpb_text_column wpb_content_element">
											<div class="wpb_wrapper">
												<p class="font-dance" style="text-align: center;">
													<b>Now identified as "bride" and "groom",<br>
														to be bound in marital bliss;<br>
														Looking upon how the love did bloom,<br>
														A beautiful journey, we reminisce.</b>
													</p>
												</div> 
											</div>
											<div class="wpb_text_column wpb_content_element ">
												<div class="wpb_wrapper">
													<p class="font-dance">
														Beginning with a mutual appreciation for TV shows with poor screenplay and a modest display of acting skills, the said bride and groom have come a long way covering common ground and getting past the uncommon. Here's an account of the endearing quirks and attributes of their betrothed from the lovebirds themselves!
													</p>
												</div> 
											</div> 
										</div> 
									</div> 
								</div>
								<div class="vc_row wpb_row vc_row-fluid">
									<div class="vc_col-sm-12 wpb_column vc_column_container">
										<div class="wpb_wrapper">
											<div class="wpb_text_column wpb_content_element">
												<div class="wpb_wrapper">
													<p class="font-dance">
														<b>The Groom:</b> Our first conversation was more akin to an interview! I remember feeling like I was on the hot seat with her pelting questions at me. When the basic checklist was all ticked, I could see that we have a lot in common including a common playlist - we share a penchant for old melodious songs, questionable Bollywood numbers, and our general world view on just about everything we spoke about! I have a healthy respect for scientists of all hues for their wonderful discoveries, but I will single out a certain Mr Bell for helping me through the Indo- Dubai communication phase!  I thought the point of arranged marriages is to find your soulmate from the limited pool of eligible single girls from your community. Little did I know that I'd be falling for one in gujju disguise in the process! I swear, she has more gujju in her to overpower any tam bone, with her tastes and commercial insights into things. I remember shocking her when as I discussed the sumptuous Hyderabadi chicken Biryani I had for lunch that day, being a proud citizen of the nawabi city. I suppose we both fell behind in conforming with our descent! I've been willing Divya for but one performance of Bharatnatyam, which I know her to be fabulous at and accomplishing that would be my immediate relationship goal, other than, of course to be eternally blessed as her husband. I'd tell you how much I love her, but no words can be louder than my repeatedly putting my life in her hands as she regards the streets of Mumbai as her own private racecourse!.
													</p>
												</div> 
											</div> 
											<div class="wpb_text_column wpb_content_element ">
												<div class="wpb_wrapper">
													<p class="font-dance">
														<b>The Bride:</b> Once I was done frightening Arjun with my initial questionnaire, I thought he's a sweet, fun bloke but one that threatens to be a morshaadam (that's tamil for curd rice -a typical tam dish, therefore an apt metaphor for a typical tam-brahm boy, "vanilla", if you will) but over the next few conversations I found that there might be more than what meets the eye. And indeed, after these months I can vouch for there being quite a strong tadka to the it! The love of my life is the greatest foodie around and one of the first things we bonded over was a common desi palate. I remember being quite appalled when I was told he's known as "Sania" by his friends, owing to his long-lived childhood admiration for Sania Mirza, whom he had the fortune of playing tennis with, and losing. Miserably. Did I mention that he's been an ace tennis player? What helped me make peace with being engaged to a Sania is knowing that he has the most sensible-for-polite company nickname amongst his friends! It's the little things, like him humming almost every second of the day, that took a while to get used to, that make me fall for him deeper. You'll always catch him singing - whether it's in the while professing his love or in the middle of a spat - replete with alakals. The most glaring instance of disparity between us would have be  our sleep cycles - he's a lark and I'm an owl! What has helped thus far is his cycle being skewed owing to the jetlag resulting from the international flight trips to Bombay, which did earn him a "Frequent Flyer" tag! I've found in him a best friend, and I can say that taking a leap of faith has never felt this safe.
													</p>
												</div> 
											</div> 
										</div> 
									</div> 
								</div>
							</div>
						</div> <!--end .post -->
						<div class="post_end"></div>
					</div>
				</div><!--end #main -->

			</div><!--end #wrapper -->
			<div id="menu-main-menu-container" class="menu-main-menu-container">
				<ul id="menu-main-menu" class="menu">
					<li class="main-menu-item post-title-1"><a href="our-story.php" class="menu-link" >Our Story</a></li>
					<li class="main-menu-item post-title-2"><a href="family.php" class="menu-link" >Our Family</a></li>
					<li class="main-menu-item post-title-3"><a href="wedding-events.php" class="menu-link" >Wedding Events</a></li>
					<li class="main-menu-item post-title-4"><a href="photos.php" class="menu-link" >Photos</a></li>
					<li class="main-menu-item post-title-5"><a href="e-vite.php" class="menu-link" >E-vite</a></li>
					<li class="main-menu-item post-title-6"><a href="engagement.php" class="menu-link" >Engagement</a></li>
				</ul>
			</div>

			<div id="footer">
				<div id="footer_content">
					<div id="footer_icon"></div>
				</div>
			</div>
			<img class="loading_icon" src="images/loading.gif" />
			<link rel='stylesheet' id='vc_hotspot_cq_style-css'  href='css/vc/style.min.css?ver=4.0' type='text/css' media='all' />
			<link rel='stylesheet' id='tooltipster-css'  href='css/vc/tooltipster.css?ver=4.0' type='text/css' media='all' />
			<script type='text/javascript' src='js/vc/jquery.tooltipster.min.js?ver=4.0'></script>
			<script type='text/javascript' src='js/vc/script.min.js?ver=4.0'></script>
		</body>
		</html>